const Product = require("../models/Product.js");

// create product by ADMIN only
module.exports.addProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})

	return newProduct.save().then((newProduct, error) => {
		if(error) {
				return error
		}
		return newProduct
		})
	}

	// validation if user isAdmin
	let message = Promise.resolve('User must be ADMIN to access this!')

	return message.then((value) => {
		return value
	})
}

// get all products
module.exports.getAllProducts = () => {
	return Course.find({}).then(result => {
		return result
	})
}

// get all ACTIVE courses only
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// get a specific product by id
module.exports.getProduct = (prodId) => {
	return Product.findById(prodId).then(result => {
		return result
	})
}

// updating a product
module.exports.updateProduct = (prodId, newData) => {
	return Product.findByIdAndUpdate(prodId, {
		name: newData.name,
		description: newData.description,
		price: newData.price
	})
	.then((updatedProduct, error) => {
		if(error) {
			return false
		}
		return true
	})
}

// archiving a product
module.exports.archiveProduct = (prodId) => {
	return Product.findByIdAndUpdate(prodId, {
		isActive: false
	})
	.then((archivedProduct, error) => {
		if(error) {
			return false
		}
		return true
	})
}