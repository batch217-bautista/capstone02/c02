const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/Product.js");

// registering user
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
}

// logging-in the user
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
		}
	})

}

// user checkout non-admin
module.exports.checkOut = async (data) => {
	// Adds the productId
	// Checks if the user is admin or not
	if (!data.isAdmin){
		let isUserCheckOut = await User.findById(data.orderId).then(user => {
			user.ordered.push({prodId : data.prodId});

			return user.save().then((user, error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			})

		})

		let isProductAdded = await Product.findById(data.prodId).then(product => {
			product.orders.push({orderId : data.orderId});

			return product.save().then((product, error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			})
		})

		if(isUserCheckOut && isProductAdded) {
			return true;	
		} else {
			return false;
		}
	}

	// If the user is admin, then return this message as a promise to avoid errors
	let message = Promise.resolve('Admins are not required to order. ROFLMAO!')

	return message.then((value) => {
		return value
	})

}

// get User details
module.exports.getDetails = data => {
	return User.findById(data.orderId).then(result => {
		return result;
	})
}