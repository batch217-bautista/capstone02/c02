const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Create a product
router.post("/create", auth.verify, (req,res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => {
		res.send(resultFromController);
	})
})

// get all product
router.get("/all", (req, res) => {
	productController.getAllProduct().then(resultFromController => {
		res.send(resultFromController)
	})
})

// get all ACTIVE products only
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => {
		res.send(resultFromController)
	})
})

// getting a specific product
router.get("/:prodId", (req, res) => {
	productController.getProduct(req.params.prodId).then(resultFromController => {
		res.send(resultFromController)
	})
})

// updating a product
router.patch("/:prodId/update", auth.verify, (req, res) => {
	productController.updateProduct(req.params.prodId, req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})

// archiving a product
router.patch("/:prodId/archive", auth.verify, (req,res) => {
	productController.archiveProduct(req.params.prodId).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

module.exports = router;