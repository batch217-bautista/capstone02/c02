const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// register/creating user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// checkout order
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		orderId: auth.decode(req.headers.authorization).id,
		prodId: req.body.prodId
	}

	userController.checkOut(data).then(resultFromController => res.send(resultFromController));
})

// retrieve User details
router.post("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getDetails({orderId : userData.id}).then(resultFromController => res.send(resultFromController));
})

module.exports = router;