const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email is required!"]
	},

	password: {
		type: String,
		required: [true, "Password is required!"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	ordered: [
		{
			prodId: {
				type : String,
				required: [true, "Product Id required"]
			}, //this was not included originally

			products: [
				{
					productName: {
						type: String,
						required: [true, "Product Name needed!"]
					},

					quantity: {
						type: Number,
						required: [true, "Total quantity needed"]
					}
				}
			],

			totalAmount: {
				type: Number,
				required: [true, "Total amount needed"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);